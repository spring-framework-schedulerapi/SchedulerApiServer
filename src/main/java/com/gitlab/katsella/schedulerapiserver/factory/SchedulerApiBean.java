package com.gitlab.katsella.schedulerapiserver.factory;

import com.gitlab.katsella.schedulerapiserver.config.SchedulerApiEnvironments;
import com.gitlab.katsella.schedulerapiserver.controller.JobStatusController;
import com.gitlab.katsella.schedulerapiserver.processor.Request.RequestProcessor;
import com.gitlab.katsella.schedulerapiserver.processor.Request.impl.RestTemplateRequestProcessor;
import com.gitlab.katsella.schedulerapiserver.repository.JobStatusRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component("SchedulerApiBean")
public class SchedulerApiBean {

    private JobStatusRepository jobStatusRepository;
    private JobStatusController jobStatusController;
    private SchedulerApiEnvironments schedulerApiEnvironments;
    private RequestProcessor requestProcessor;

    public SchedulerApiBean(JobStatusRepository jobStatusRepository, JobStatusController jobStatusController, SchedulerApiEnvironments schedulerApiEnvironments,
                            @Nullable RequestProcessor requestProcessor) {
        this.jobStatusRepository = jobStatusRepository;
        this.jobStatusController = jobStatusController;
        this.schedulerApiEnvironments = schedulerApiEnvironments;

        if (Objects.isNull(requestProcessor))
            this.requestProcessor = new RestTemplateRequestProcessor();
        else
            this.requestProcessor = requestProcessor;
    }

    public JobStatusRepository getJobStatusRepository() {
        return jobStatusRepository;
    }

    public JobStatusController getJobStatusController() {
        return jobStatusController;
    }

    public SchedulerApiEnvironments getSchedulerApiEnvironments() {
        return schedulerApiEnvironments;
    }

    public RequestProcessor getRequestProcessor() {
        return requestProcessor;
    }
}
