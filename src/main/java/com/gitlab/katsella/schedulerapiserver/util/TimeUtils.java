package com.gitlab.katsella.schedulerapiserver.util;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

public class TimeUtils {
    public static Long deltaTimeSecond(LocalDateTime firstDate, LocalDateTime secondDate) {
        return firstDate.toEpochSecond(ZoneOffset.UTC) - secondDate.toEpochSecond(ZoneOffset.UTC);
    }

    public static Long deltaTimeMillis(LocalDateTime firstDate, LocalDateTime secondDate) {
        return ChronoUnit.MILLIS.between(firstDate, secondDate);
    }
}