package com.gitlab.katsella.schedulerapiserver.processor.Scheduler.impl;

import com.gitlab.katsella.schedulerapiserver.core.exception.ScheduledJobNotFound;
import com.gitlab.katsella.schedulerapiserver.entity.JobStatus;
import com.gitlab.katsella.schedulerapiserver.processor.Request.RequestProcessor;
import com.gitlab.katsella.schedulerapiserver.processor.Request.model.RequestModel;
import com.gitlab.katsella.schedulerapiserver.processor.Scheduler.ServiceScheduler;
import com.gitlab.katsella.schedulerapiserver.processor.Scheduler.model.ScheduledModel;
import com.gitlab.katsella.schedulerapiserver.processor.Scheduler.model.SchedulerApiBody;
import com.gitlab.katsella.schedulerapiserver.processor.Scheduler.model.SchedulerParams;
import com.gitlab.katsella.schedulerapiserver.service.IJobStatusService;
import com.gitlab.katsella.schedulerapiserver.util.MapperUtils;
import com.gitlab.katsella.schedulerapiserver.util.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpStatusCodeException;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServiceSchedulerProcessor implements ServiceScheduler {

    private static final Logger LOGGER = Logger.getLogger(ServiceSchedulerProcessor.class.getName());

    private RequestProcessor requestProcessor;

    private IJobStatusService jobStatusService;

    private Map<String, ScheduledModel> scheduled = new HashMap<>();

    private final TaskScheduler taskExecutor;

    private final long MILLI_TO_MICRO_MULTIPLIER = 1000000;

    private final String endpointUrl;

    private final String serviceName;

    public ServiceSchedulerProcessor(String endpointUrl, String serviceName, TaskScheduler taskScheduler, RequestProcessor requestProcessor, IJobStatusService jobStatusService) {
        this.endpointUrl = endpointUrl;
        this.serviceName = serviceName;
        this.taskExecutor = taskScheduler;
        this.requestProcessor = requestProcessor;
        this.jobStatusService = jobStatusService;
    }

    public ServiceSchedulerProcessor triggerFixedRate(String uniqueName, String jobName, long initialDelay, long period, long timeout) {
        checkNewSchedulerValidation(uniqueName, jobName);

        SchedulerParams params = new SchedulerParams(uniqueName, jobName, false, timeout);
        Runnable task = triggerTask(params);
        ScheduledFuture future = taskExecutor.scheduleAtFixedRate(task, Date.from(LocalDateTime.now().plusNanos(initialDelay * MILLI_TO_MICRO_MULTIPLIER)
                .atZone(ZoneId.systemDefault()).toInstant()), period);
        scheduled.put(uniqueName, new ScheduledModel(task, params, future));
        return this;
    }

    public ServiceSchedulerProcessor triggerFixedRate(String uniqueName, String jobName, long period, long timeout) {
        return triggerFixedRate(uniqueName, jobName, 0, period, timeout);
    }

    public ServiceSchedulerProcessor triggerFixedDelay(String uniqueName, String jobName, long initialDelay, long period, long timeout) {
        checkNewSchedulerValidation(uniqueName, jobName);

        SchedulerParams params = new SchedulerParams(uniqueName, jobName, true, timeout);
        Runnable task = triggerTask(params);
        ScheduledFuture future = taskExecutor.scheduleWithFixedDelay(task, Date.from(LocalDateTime.now().plusNanos(initialDelay * MILLI_TO_MICRO_MULTIPLIER)
                .atZone(ZoneId.systemDefault()).toInstant()), period);
        scheduled.put(uniqueName, new ScheduledModel(task, params, future));
        return this;
    }

    public ServiceSchedulerProcessor triggerFixedDelay(String uniqueName, String jobName, long period, long timeout) {
        return triggerFixedDelay(uniqueName, jobName, 0, period, timeout);
    }

    public ServiceSchedulerProcessor triggerCron(String uniqueName, String jobName, String cronExpression, long timeout) {
        checkNewSchedulerValidation(uniqueName, jobName);

        SchedulerParams params = new SchedulerParams(uniqueName, jobName, false, timeout);
        Runnable task = triggerTask(params);
        ScheduledFuture future = taskExecutor.schedule(task, new CronTrigger(cronExpression));
        scheduled.put(uniqueName, new ScheduledModel(task, params, future));
        return this;
    }

    private Runnable triggerTask(SchedulerParams params) {
        return () -> {
            try {
                if (params.isLockTask() && !params.getLock().tryLock()) {
                    LOGGER.warning(MessageFormat.format("triggerTask::warn Scheduled job triggered while running. serviceType: {0}, endpointUrl: {1}, uniqueName: {2}, jobName: {3}", serviceName, endpointUrl, params.getUniqueName(), params.getJobName()));
                    return;
                }

                requestExecute(params);
            } catch (Exception e) {
                params.setLastTriggeredTime(null);
                LOGGER.log(Level.SEVERE, MessageFormat.format("triggerTask::error serviceType: {0}, endpointUrl: {1}, uniqueName: {2}, jobName: {3}", serviceName, endpointUrl, params.getUniqueName(), params.getJobName()), e);
            } finally {
                if (params.isLockTask())
                    params.getLock().unlock();
            }
        };
    }

    @Transactional
    void requestExecute(SchedulerParams params) {
        boolean isExist = jobStatusService.isJobExist(serviceName, params.getJobName(), params.getTimeout());
        if (isExist) {
            LOGGER.log(Level.SEVERE, MessageFormat.format("requestExecute::error Job already executed. SchedulerParams: {0}", MapperUtils.writeValueAsStringDateFormattedWithoutException(params)));
            return;
        }
        JobStatus jobStatus = jobStatusService.jobCreate(serviceName, params.getJobName());
        request(params, jobStatus.getId());
    }

    private void request(SchedulerParams params, long jobId) {
        try {
            SchedulerApiBody schedulerApiBody = new SchedulerApiBody(params.getJobName(), jobId);
            RequestModel<SchedulerApiBody> requestModel = new RequestModel();
            requestModel.setBody(schedulerApiBody);
            requestModel.setUrl(endpointUrl);
            requestProcessor.post(requestModel, String.class);
        } catch (HttpStatusCodeException e) {
            if (e.getStatusCode() == HttpStatus.GATEWAY_TIMEOUT)
                return;
            else
                throw e;
        }
    }

    public void run(String uniqueName) {
        ScheduledModel model = getScheduledModel(uniqueName);
        model.getRunnable().run();
    }

    public void cancel(String uniqueName) {
        ScheduledModel model = getScheduledModel(uniqueName);
        model.getScheduledFuture().cancel(false);
    }

    private ScheduledModel getScheduledModel(String uniqueName) {
        if (StringUtils.isBlank(uniqueName))
            throw new IllegalArgumentException("uniqueName is empty serviceType: " + serviceName);

        ScheduledModel model = scheduled.get(uniqueName);
        if (Objects.isNull(model))
            throw new ScheduledJobNotFound("ScheduledModel not found. serviceType: " + serviceName + "  uniqueName: " + uniqueName);

        return model;
    }

    private void checkNewSchedulerValidation(String uniqueName, String jobName) {
        if (StringUtils.isBlank(uniqueName))
            throw new IllegalArgumentException("uniqueName is null or empty. serviceType: " + serviceName);

        if (scheduled.containsKey(uniqueName))
            throw new IllegalArgumentException("uniqueName is exist. serviceType: " + serviceName + " uniqueName: " + uniqueName);

        if (StringUtils.isBlank(jobName))
            throw new IllegalArgumentException("jobName is null or empty. serviceType: " + serviceName);
    }
}
