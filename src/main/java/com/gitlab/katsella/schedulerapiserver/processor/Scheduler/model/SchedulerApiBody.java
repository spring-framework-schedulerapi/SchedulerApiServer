package com.gitlab.katsella.schedulerapiserver.processor.Scheduler.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SchedulerApiBody {
    private String name;
    private Long jobId;

    public SchedulerApiBody(
            @JsonProperty("name")
                    String name, @JsonProperty("jobId") Long jobId) {
        this.name = name;
        this.jobId = jobId;
    }

    public String getName() {
        return name;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }
}
