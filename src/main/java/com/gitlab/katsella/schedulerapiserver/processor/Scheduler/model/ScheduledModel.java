package com.gitlab.katsella.schedulerapiserver.processor.Scheduler.model;

import java.util.concurrent.ScheduledFuture;

public class ScheduledModel {
    private Runnable runnable;
    private ScheduledFuture scheduledFuture;
    private SchedulerParams params;

    public ScheduledModel(Runnable runnable, SchedulerParams params, ScheduledFuture scheduledFuture) {
        this.runnable = runnable;
        this.params = params;
        this.scheduledFuture = scheduledFuture;
    }

    public Runnable getRunnable() {
        return runnable;
    }

    public ScheduledFuture getScheduledFuture() {
        return scheduledFuture;
    }

    public SchedulerParams getParams() {
        return params;
    }
}
