package com.gitlab.katsella.schedulerapiserver;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;

@EnableAutoConfiguration
@Configuration
public class SchedulerApiServer {
    public static void main(String[] args) {
    }
}
