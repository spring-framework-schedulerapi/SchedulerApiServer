package com.gitlab.katsella.schedulerapiserver.core.exception.handler;

import com.gitlab.katsella.schedulerapiserver.core.exception.ScheduledJobNotFound;
import com.gitlab.katsella.schedulerapiserver.core.exception.model.ErrorDetails;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger LOGGER = Logger.getLogger(GlobalExceptionHandler.class.getName());

    private final String MESSAGE_PREFIX = "";

    @org.springframework.web.bind.annotation.ExceptionHandler(ScheduledJobNotFound.class)
    public ResponseEntity<?> scheduledJobNotFoundExceptionHandling(ScheduledJobNotFound exception, WebRequest request) {
        LOGGER.log(Level.SEVERE, "ScheduledJobNotFoundExceptionHandling", exception);
        return new ResponseEntity<>(new ErrorDetails(LocalDateTime.now(), MESSAGE_PREFIX + exception.getMessage(), request.getDescription(false)), HttpStatus.NOT_FOUND);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    public ResponseEntity<?> globalExceptionHandling(Exception exception, WebRequest request) {
        LOGGER.log(Level.SEVERE, "GlobalExceptionHandling", exception);
        return new ResponseEntity<>(new ErrorDetails(LocalDateTime.now(), MESSAGE_PREFIX + exception.getCause().getMessage(), request.getDescription(false)), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(RuntimeException.class)
    public ResponseEntity<?> globalRuntimeExceptionHandling(Exception exception, WebRequest request) {
        LOGGER.log(Level.SEVERE, "GlobalRuntimeExceptionHandling", exception);
        return new ResponseEntity<>(new ErrorDetails(LocalDateTime.now(), MESSAGE_PREFIX + exception.getMessage(), request.getDescription(false)), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
