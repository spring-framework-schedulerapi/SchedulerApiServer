package com.gitlab.katsella.schedulerapiserver.core.exception;

public class ScheduledJobNotFound extends RuntimeException {
    public ScheduledJobNotFound(String message) {
        super(message);
    }
}
