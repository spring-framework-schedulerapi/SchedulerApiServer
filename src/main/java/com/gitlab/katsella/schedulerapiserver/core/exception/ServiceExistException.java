package com.gitlab.katsella.schedulerapiserver.core.exception;

public class ServiceExistException extends RuntimeException {
    public ServiceExistException(String message) {
        super(message);
    }
}
